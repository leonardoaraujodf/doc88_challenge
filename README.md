# doc88_challenge

# About

This repository has the files and contents about the firmware challenge DOC-88.

*********
# Contents
1. Challenge Questions
2. What is my application and what it does
3. How to install it
4. How to run it
5. Solving some issues
6. Useful Links
*********

# Questions 

1. Utilizando um sistema operacional linux escreva um programa em python com duas threads. Na primeira thread, gere uma onda senoidal com amplitude, nível DC e frequência configuráveis. Adicione também uma opção de gerar um ruído branco na senoide com sua intensidade configurável. Amostre o sinal e envie essas amostras para seu setup físico (e.g. via porta serial USB ou via pipe) com uma taxa de amostragem também configurável.
2. Utilizando um setup físico de sua escolha (o próprio computador ou um microcontrolador), faça um código RTOS em C que leia esses dados e desenvolva um algoritmo que estime a frequência e amplitude do sinal em tempo real. Envie esses 2 dados para um servidor broker mqtt (local ou público) via WiFi a cada 1 segundo.
3. No mesmo programa em python, na segunda thread, escreva um programa que receba esses 2 dados (frequência e amplitude) via MQTT. Por fim, faça o programa em python plotar esses dados em 2 gráficos de linha diferentes, em tempo real. Cada gráfico terá 2 variáveis: a real gerada pelo programa e a estimada pelo seu código RTOS. Plote também o gráfico da senoide amostrada no tempo. 
4. Faça uma documentação que explique como rodar sua aplicação.

# What is my application and what it does 

In this program we can generate a discrete sinusoid and see its properties in real time. Using the serial communication with the PC-USB via UART, it send its samples to the microcontroller ESP32, which is responsible to estimate the signal amplitude and frequency.

The ESP32 microcontroller has two Xtensa dual-core 32 bit LX6 microprocessor. In these application, we use the main core (core 1) to serial communication and to estimate the signal amplitude and frequency of the incoming samples using the **Fast-Fourier Transform** - FFT algorithm. The core 0 is used to publish in the MQTT broker. It makes use of the **freeRTOS library** in the Arduino IDE.

The ESP32 sent its estimated values to a MQTT broker, two values at a second (signal amplitude and signal frequency). 

The PC then connects to the MQTT broker, subscribes to the topic and then receives this estimated values. 

After that, it plots the estimated amplitude vs real amplitude and also the estimated signal frequency vs real signal frequency. There is also a plot of the real time sinusoid signal. The Figure 1 shows how is the plot.

![graphs](/fig/graphs.png)
**Figure 1** - A signal been sent with a frequency of 60 Hz, amplitude 1, sampling frequency of 10 kHz and 1024 samples for the FFT. There's also a AWGN inserted which ranges from 0 to 0.1 times the signal amplitude

# How to install it 

**Caution**: 
* The steps above are for linux users only. 
* DO NOT try to install the Python libraries first, or it may cause some problems to run the main application. First, follow the steps described for the Arduino IDE and then install the Python libraries. 

## Arduino IDE

* If you don't have the Arduino IDE installed in your computer, just follow this tutorial provided by the Arduino guys:

`https://www.arduino.cc/en/guide/linux`

* Then, you need to install the ESP32 Board files into the Arduino IDE. Follow the steps in the tutorial below. You will see that there are some steps that uses Python 2. I just followed it and I didn't have any issues.

`https://circuits4you.com/2018/02/02/installing-esp32-board-in-arduino-ide-on-ubuntu-linux/`

* For this application we need the libraries **WiFi.h**, **PubSubClient.h** and **arduinoFFT.h**. Just follow this tutorial if you don't know how to install:

`https://www.arduino.cc/en/guide/libraries`

## Python Packages

* Pip will help you install some packages. First download `pip` from:

`wget https://bootstrap.pypa.io/get-pip.py`

* Then install pip using python3:

`python3 get-pip.py`

* In the next step, we will install numpy. Numpy will helps us make our vector operations, cause it is a powerful N-dimensional array object. Install `numpy` using:

`python3 -m pip install numpy`

* Matplotlib will help us make our graphs. Install `matplotlib` using:

`python3 -m pip install matplotlib`

* We need also a library in Python to handle the serial port operations. For this we have **PySerial**. Install it using:

`pip3 install pyserial`

* Also, we need to install a library to handle the MQTT. For this we have Paho-MQTT. Install `paho-mqtt` using: 

`pip3 install paho-mqtt`

# How to run it

The source code located in folder `src/mqtt_esp32` called [mqtt_esp32.ino](/src/mqtt_esp32/mqtt_esp32.ino) should be uploaded to the ESP32. Install the required libraries (see the section **How to install it** in this README). Also, you need to change your network name and password:

```
// Network name
const char *ssid = "motog5";
// Network password
const char *password = "12345678";
```
To go to the main application, you need the to run source code in Python, called [challenge.py](/src/python/challenge.py) using:

`python3 challenge.py`

Be careful with serial port permissions (see **6. Solving some issues**).

When the code runs, the menu showed in the Figure 2 appears and also the graph showed in the Figure 1. To ensure that some data is been received, push the button EN in the ESP32. Also verifies if you are really connected to a Wi-Fi network!

We can set various different settings about the signal to be transmitted in a **user-friendly menu**, showed in the Figure 2.

![menu](/fig/menu.png)

**Figure 2** - Selectable menu in the main application

## How to use the main menu

There are seven different choosable properties:
* **Option 0** - Close program: When the user tips 0 + ENTER, the main program closes.
* **Option 1** - Select a new sampling frequency: This option can select a new sampling frequency to be sent over the PC to the ESP32. Just type 1, type the sampling frequency and hit ENTER. The sampling frequency that can be selected range from 1 to 1000000 Hz (I'm not sure that you can use a sampling frequency so high!). 
* **Option 2** - Select a new number of samples to the FFT: To calculate the FFT, we need to choose how many samples of our signal the algorithm needs. Depending of the samples number, our estimation about the signal frequency response can become better or not. The selectable number of samples range from 2 to 32768.
* **Option 3** - Select a new signal amplitude: The selectable range for the signal amplitude are 0.01 and 1000000. 
* **Option 4** - Select a new signal frequency: The minimum signal frequency allowed to be inserted is 0.1 Hz. The maximum signal frequency depends of the sampling frequency choosed and the application don't allow you to select a signal frequency which is higher than twice the sampling frequency (remember the Nyquist theorem!).
* **Option 5** - Select a different DC level: A DC level can be inserted in the signal. The range varies between 0 and 1000000.
* **Option 6** - Select a AWGN: The user can select a AWGN noise to be inserted in the signal and verify that the algorithm can even estimate the amplitude and signal frequency over low signal-to-noise ratios. The noise range varies from 0 to 100 times of the amplitude signal. 

# Solving some issues

1. If you are having issues with your serial port, try the following command (first try to see the name of your serial port, e.g. mine is `ttyUSB0`):

`sudo chmod a+rw /dev/ttyUSB0`

# Useful Links

* How to program ESP-32 using **tasks**:
`https://portal.vidadesilicio.com.br/multiprocessamento-esp32/`

* How to Connect ESP32 to **MQTT Broker**:
`https://iotdesignpro.com/projects/how-to-connect-esp32-mqtt-broker`
`https://www.arduinoecia.com.br/enviando-mensagens-mqtt-modulo-esp32-wifi/`