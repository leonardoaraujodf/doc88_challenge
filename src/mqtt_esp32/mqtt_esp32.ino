#include <WiFi.h>
#include <PubSubClient.h>
#include <arduinoFFT.h>

// Network name
const char *ssid = "motog5";
// Network password
const char *password = "12345678";
const char *mqtt_server = "m12.cloudmqtt.com";
const int mqtt_port = 15299;
const char *mqtt_user = "wawkqqpw";
const char *mqtt_password = "0Vesn5GNKywV";

WiFiClient esp_client;
PubSubClient client(esp_client);

int counter = 1;
char msg[100] = {0};

arduinoFFT FFT = arduinoFFT(); // Create FFT objects

// number of samples used
uint16_t number_samples = 0;
uint16_t received_samples = 0;
double sampling_frequency = 0;
double signal_value = 0;
double estimated_signal_amplitude = 0;
double estimated_signal_frequency = 0;
double mean_signal_amplitude = 0;
double mean_signal_frequency = 0;


static double *signal_samples;
static double *v_real;
static double *v_imag;
char information[50] = {0};

bool allocate_data(){
  if ((received_samples == 0) && (number_samples == 0)){
    return false;
  }
  else{
    if (received_samples != number_samples){
      number_samples = received_samples;
      delete signal_samples;
      delete v_real;
      delete v_imag;
      signal_samples = new double[number_samples]();
      v_real = new double[number_samples]();
      v_imag = new double[number_samples]();  
    }
    return true;
  }
}

void calculate_fft(){
  
  if(allocate_data()){
    for (uint16_t i = number_samples-1; i > 0; i--){
        signal_samples[i] = signal_samples[i-1];
    }
    signal_samples[0] = signal_value;
  //
    for (uint16_t i = 0; i < number_samples; i++){
      v_real[i] = signal_samples[i];
      v_imag[i] = 0.0;
      //sprintf(s_value,"Re: %lf\n",v_real[i]);
      //Serial.write(s_value);
      //sprintf(s_value,"Im: %lf\n",v_imag[i]);
      //Serial.write(s_value);
    }
  
    FFT.Windowing(v_real, number_samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
    FFT.Compute(v_real, v_imag, number_samples, FFT_FORWARD);
    FFT.ComplexToMagnitude(v_real, v_imag, number_samples);
    FFT.MajorPeak(v_real, number_samples, sampling_frequency, &estimated_signal_frequency, &estimated_signal_amplitude);
    //sprintf(information, "X: %lf f: %lf %d %lf\n", (4*estimated_signal_amplitude)/number_samples, estimated_signal_frequency, (int)number_samples, sampling_frequency);
    //Serial.write(information);
  
    if(mean_signal_amplitude == 0){
      mean_signal_amplitude = (4*estimated_signal_amplitude)/number_samples;
      mean_signal_frequency = estimated_signal_frequency;
    }
    else{
      mean_signal_amplitude = (mean_signal_amplitude + (4*estimated_signal_amplitude)/number_samples)/2;
      mean_signal_frequency = (mean_signal_frequency + estimated_signal_frequency)/2;
    }
  }
}

bool verify_signal_received(float value){
  if(isnan(value) || isinf(value) || (value > 1000000) || (value < -1000000)){
    return false;
  }
  return true;
}

bool verify_sample_frequency_received(float value){
  if(isnan(value) || isinf(value) || (value < 1) || (value > 1000000)){
    return false;
  }
  return true;
}

bool verify_number_samples_received(float value){
  if(isnan(value) || isinf(value) || (value < 1) || (value > 32768)){
    return false;
  }
  return true;
}

float receive_float(){
  unsigned char received_byte = 0, store_recv_bytes[4] = {0};
  float received_data = 0;
  char s_received_data[20] = {0};
  
  // Turn the received data bytes into 0
  for(int i = 0; i < 4; i++){
    store_recv_bytes[i] = Serial.read();
  }
  // Write the float bytes into the memory space
  // which the received_data variable is allocated
  memcpy(&received_data, store_recv_bytes, sizeof(float));
  // Send back to see if everything is ok
  //sprintf(s_received_data,"%f\n",received_data);
  //Serial.write(s_received_data);
  return received_data;
}

void verify_received_data(){
  if(Serial.available() > 0){
    //sprintf(information, "OK!\n");
    //Serial.write(information);
    //Read the incoming byte
    unsigned char received_byte = Serial.read();
    float received_float = 0;
    // Verify first if the data to come is a information about the sampling frequency or a signal data
    // When 'd' is received, signal data is coming. 
    // When 's' is received, information about sampling frequency is coming
    // When 's' is received, information about number of samples for the fft is coming
    
    if(received_byte == 'd'){
        received_float = receive_float();
        if (verify_signal_received(received_float)){
          Serial.write("a\n");
          signal_value = received_float;
          calculate_fft();
        }
        else{
          Serial.write("n\n");
        }
    }
    else if(received_byte == 'f'){
        received_float = receive_float();
        if (verify_sample_frequency_received(received_float)){
          Serial.write("a\n");
          sampling_frequency = received_float;
        }
        else{
          Serial.write("n\n");
        }
    }    
    else if(received_byte == 's'){
        received_float = receive_float();
        if (verify_number_samples_received(received_float)){
          Serial.write("a\n");
          received_samples = (uint16_t)received_float;
        }
        else{
          Serial.write("n\n");
        }
    }
  }
}

void reconnect_broker(){
  client.setServer(mqtt_server, mqtt_port);
  while(!client.connected()){
    Serial.write("Connecting to the broker MQTT...\n");
    if(client.connect("ESP32Client", mqtt_user, mqtt_password)){
      Serial.write("Connected to the broker!\n");
    }
    else{
      sprintf(msg,"Fail to establish connection to the broker: %d.\n",client.state());
      Serial.write(msg);
      delay(2000);
    }
  }
}

void send_message_mqtt(void *param) {
  while(1){
    reconnect_broker();
    if(number_samples > 0){
      sprintf(msg,"%lf, %lf\n",(4*estimated_signal_amplitude)/number_samples,estimated_signal_frequency);  
    }
    else{
      sprintf(msg,"%lf, %lf\n", estimated_signal_amplitude, estimated_signal_frequency);
    }

    
  
    // Send the message to the broker
    client.publish("desafio-doc88-leonardo",msg);
    
    //Serial.write("Message sent: ");
    //Serial.write(msg);
  
    //counter++;
    //if (counter == 100){
    //  counter = 0;
    //}
    delay(1000);
  }
}

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.write("Initializing network connection...\n");
  }
  Serial.println("Connection established.");
  //Create the task "verify_received_data()" with priority 1, assigned to core 0
  xTaskCreatePinnedToCore(send_message_mqtt, "Task2", 8192, NULL, 1, NULL, 0);
  delay(1);
}

void loop(){//loop is assigned to core 1 automatically by the system
  verify_received_data();
}
