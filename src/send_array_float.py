import serial

ser = serial.Serial('/dev/ttyUSB0', baudrate = 115200, timeout = 1)

def send_float(value):
    # Convert the value in float to string
    s_value = f"{value}"
    # Take the length of the float word
    value_len = len(s_value)
    # Convert the length of the float string to string
    s_value_len = f"{value_len}"
    # Send 's' to start the communication
    ser.write(b's')
    # Convert the string s_value_len to bytes
    byte_value = bytes(s_value_len,'utf-8')
    # Send the bytes
    ser.write(byte_value)
    # Send 'd' to say that data bytes is coming
    ser.write(b'd')
    # Convert the data string to bytes
    byte_value = bytes(s_value,'utf-8')
    # Send the data
    ser.write(byte_value)
    esp_data = ser.readline()
    return esp_data


while 1:
    user_input = input('> Do you want to send data? Ans: y or n ')
    if user_input == 'y':
        user_input = input('> Type a number: ')
        print(send_float(user_input))
    elif user_input == 'n':
        break