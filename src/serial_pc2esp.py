import serial
import numpy as np
import time
import csv
ser = serial.Serial('/dev/ttyUSB0', baudrate = 115200, timeout = 1)

def send_float(value):
    # Convert the value to bytes
    byte_value = bytes(np.float32(value))
    # Send the bytes
    ser.write(byte_value)
    # esp_data = ser.readline()
    # return esp_data

def send_float_data(value):
    # Send 'd' to start the communication
    # 'd' stands for data
    ser.write(b'd')
    # Send the data
    send_float(value)

def send_sampling_frequency(value):
    # Send 'f' to start the communication
    ser.write(b'f')
    # Send the data
    send_float(value)

def send_number_samples(value):
    # Send 's' to start the communication
    ser.write(b's')
    # Send the data
    send_float(value)

# sampling frequency
sampling_frequency = 100.0

# sampling time
sampling_time = 1/sampling_frequency

# signal amplitude
amplitude = 1

# signal frequency
signal_frequency = 10

# signal dc level
signal_dc_level = 0

# signal period
signal_period = 1/signal_frequency

# number samples per cicle
number_samples_per_cicle = int(sampling_frequency/signal_frequency)

# number samples to plot
number_samples_plot = 2*number_samples_per_cicle

# number samples to calculate the fft
number_samples_fft = 4*number_samples_per_cicle
allowed_samples_fft = 1
while allowed_samples_fft < number_samples_fft:
    allowed_samples_fft *= 2

number_samples_fft = allowed_samples_fft

# actual time
actual_time = 0

# noise range
noise_range = 0.5*amplitude

# Initialize a list of values with zeros
# This list will be used to store the signal values
# in a file and then plot the real time signal
list_signal = [0]*number_samples_plot
list_time = np.arange(0,number_samples_plot)*sampling_time

# Store each noise component squared
noise_squared = 0

# Store each signal component squared
signal_squared = 0

while 1:
    # send the sampling frequency used for each cycle
    if actual_time == 0:
        send_sampling_frequency(sampling_frequency)
        send_number_samples(number_samples_fft)

    # calculate the signal value
    signal = amplitude*np.sin(2*np.pi*signal_frequency*actual_time) + signal_dc_level
    # calculate the noise value
    noise = np.float64(np.random.normal(-noise_range,noise_range,1))
    # add signal with the white noise
    final_signal = signal + noise
    # calculate the actual signal value squared
    signal_squared = signal_squared + signal**2
    # calculate the actual signal noise squared
    noise_squared = noise_squared + noise**2

    # pop the last signal value from the list
    list_signal.pop()
    # insert the new signal value calculated
    list_signal.insert(0,final_signal)

    # increment the time variable
    actual_time = actual_time + sampling_time

    # verify if actual_time is greater or equal
    # to one signal period
    if actual_time >= (signal_period - sampling_time):
        # turn the time variable zero
        actual_time = 0
        # calculate the signal noise ratio in dB
        snr = 10*np.log10(signal_squared/noise_squared)
        # turn signal_squared and noise_squared zero
        signal_squared = 0
        noise_squared = 0
        # print the value of signal to noise ratio
        # print(f"SNR = {snr}")
        file_id = open("sent_signal.txt","w+")
        for i in range(len(list_signal)):
            file_id.write(f"{list_time[i]},{list_signal[i]}\n")
        file_id.close()


    # send the signal data
    send_float_data(final_signal)
    # print the esp data about estimated signal frequency and amplitude
    print(ser.readline())
    # sleep the sampling time
    time.sleep(sampling_time)
        


