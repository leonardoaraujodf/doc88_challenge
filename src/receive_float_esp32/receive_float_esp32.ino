
unsigned char received_byte, store_recv_bytes[4];
float received_data;
char s_received_data[20] = {0};

void setup(){
  Serial.begin(115200);
}

void loop(){
  if(Serial.available() > 0){
    received_byte = Serial.read();
    // If a 'd' is received, then the communication starts
    if(received_byte == 'd'){
      // Turn the received_data bytes into 0
      received_data = 0x0000;
      // Receive and shift the data
      for(int i = 0; i < 4; i++){
        store_recv_bytes[i] = Serial.read();
        
      }

      memcpy(&received_data, store_recv_bytes, sizeof(float));
      //received_data = store_recv_bytes[3]|store_recv_bytes[2]|store_recv_bytes[1]|store_recv_bytes[0];
      
      sprintf(s_received_data,"%f\n",received_data);
      Serial.write(s_received_data);
    }
  }
}
