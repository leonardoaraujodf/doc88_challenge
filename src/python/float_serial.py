import serial
import numpy as np

MY_SERIAL_PORT = "/dev/ttyUSB0"
MY_BAUD_RATE = 115200
MY_TIME_OUT = 1

ser = serial.Serial(MY_SERIAL_PORT, baudrate=MY_BAUD_RATE, timeout=MY_TIME_OUT)


def send_float(value):
    # Convert the value to bytes
    byte_value = bytes(np.float32(value))
    # Send the bytes
    ser.write(byte_value)


def send_float_data(value):
    # Send 'd' to start the communication
    # 'd' stands for data
    ser.write(b'd')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'd')
        send_float(value)
        esp_data = ser.readline()


def send_sampling_frequency(value):
    # Send 'f' to start the communication
    ser.write(b'f')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'f')
        send_float(value)
        esp_data = ser.readline()


def send_number_samples(value):
    # Send 's' to start the communication
    ser.write(b's')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'f')
        send_float(value)
        esp_data = ser.readline()
