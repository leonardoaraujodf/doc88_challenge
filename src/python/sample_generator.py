import float_serial
import time
import numpy as np

# Define how many cicles from signal will be showed in the plot
NUMBER_CICLES_TO_PLOT = 2
WAIT_TIME_FOR_MAIN_MENU = 1
NUMBER_RECEIVED_SAMPLES_TO_PLOT = 10

def read_user_values():
    read_data = open("menu_values.txt", "r").read()
    data_array = read_data.split("\n")

    close_program = int(data_array[0])
    sampling_frequency = float(data_array[1])
    number_samples_fft = int(data_array[2])
    signal_amplitude = float(data_array[3])
    signal_frequency = float(data_array[4])
    signal_dc_level = float(data_array[5])
    noise_range = float(data_array[6])

    list_user_values = [close_program, sampling_frequency, number_samples_fft,
                        signal_amplitude, signal_frequency, signal_dc_level, noise_range]
    return list_user_values


def read_signal_samples():
    file_id = open("sent_signal.txt", "r").read()
    data_read = file_id.split('\n')
    x_values = []
    y_values = []
    data_read.pop()
    for each_line in data_read:
        x,y = each_line.split(',')
        x_values.append(np.float64(x))
        y_values.append(np.float64(y))
    return (x_values, y_values)


def write_signal_samples(x_values, y_values):
    file_id = open("sent_signal.txt", "w+")
    for index in range(len(y_values)):
        file_id.write(f"{x_values[index]},{y_values[index]}\n")
    file_id.close()

def initialize_sent_samples_file(signal_amplitude, signal_frequency):
    file_id = open("expected_samples.txt","w+")
    for index in range(NUMBER_RECEIVED_SAMPLES_TO_PLOT):
        file_id.write(f"{signal_amplitude},{signal_frequency}\n")
    file_id.close()

def samples_generator():
    time.sleep(WAIT_TIME_FOR_MAIN_MENU)
    actual_time = 0
    close_program = 0
    program_initialize_first_time = 0
    signal_squared = 0
    noise_squared = 0
    # send ten times the signal_frequency and number_samples
    # to ensure that the esp received this values

    while close_program == 0:
        [close_program, sampling_frequency, number_samples_fft, signal_amplitude,
            signal_frequency, signal_dc_level, noise_range] = read_user_values()

        sampling_time = 1/sampling_frequency
        signal_period = 1/signal_frequency
        number_samples_per_cicle = int(sampling_frequency/signal_frequency)
        number_samples_to_plot = NUMBER_CICLES_TO_PLOT*number_samples_per_cicle

        if actual_time == 0:
            float_serial.send_sampling_frequency(sampling_frequency)
            float_serial.send_number_samples(number_samples_fft)

        # generate the new sample
        signal = signal_amplitude * \
            np.sin(2*np.pi*signal_frequency*actual_time) + signal_dc_level

        noise = np.float64(np.random.normal(-noise_range, noise_range, 1))
        # add white noise to the sample generated
        final_signal = signal + noise
        signal_squared = signal_squared + signal**2
        noise_squared = noise_squared + noise**2

        # verify if the program was initialized the first time
        if program_initialize_first_time == 0:
            for x in range(10):
                float_serial.send_sampling_frequency(sampling_frequency)
                float_serial.send_number_samples(number_samples_fft)

            initialize_sent_samples_file(signal_amplitude, signal_frequency)
            list_time = [0]*number_samples_to_plot
            list_signal = [0]*number_samples_to_plot
            write_signal_samples(list_time, list_signal)
            program_initialize_first_time = 1
        else:
            # verify if the user has changed the sample frequency
            initialize_sent_samples_file(signal_amplitude, signal_frequency)
            list_time_read, list_signal_read = read_signal_samples()
            if len(list_time_read) != number_samples_to_plot:
                list_signal = [0]*number_samples_to_plot
                list_time = [0]*number_samples_to_plot
            else:
                list_time = list_time_read
                list_signal = list_signal_read

        list_time.pop()
        list_time.insert(0, actual_time)
        list_signal.pop()
        list_signal.insert(0, final_signal)

        write_signal_samples(list_time, list_signal)
        
        actual_time = actual_time + sampling_time
        
        if actual_time >= (signal_period - sampling_time):
            actual_time = 0
            # calculate the signal noise ratio in dB
            snr = 10*np.log10(signal_squared/noise_squared)
            signal_squared = 0
            noise_squared = 0

        float_serial.send_float_data(final_signal)
        time.sleep(sampling_time)
