import sys
import threading
import graphs
import menu
import mqtt
import sample_generator


def main():
    try:
        thread1 = threading.Thread(name='sample_generator', target=sample_generator.samples_generator, daemon=True)
        thread2 = threading.Thread(name='mqtt', target=mqtt.mqtt_handler, daemon=True)
        # thread3 = threading.Thread(name='graphs', target=graphs.plot_graphs, daemon=True)
        thread1.start()
        thread2.start()
        # thread3.start()
        menu.main_menu()
        raise KeyboardInterrupt
    except KeyboardInterrupt:
        print("Finishing the program...")


if __name__ == '__main__':
    main()
