import numpy as np
import paho.mqtt.client as mqtt
import time


USERNAME = "wawkqqpw"
PASSWORD = "0Vesn5GNKywV"
BROKER_NAME = "m12.cloudmqtt.com"
BROKER_PORT_NUMBER = 15299
BROKER_COMMUN_TIMEOUT = 60
BROKER_TOPIC = "desafio-doc88-leonardo"
WAIT_PROGRAM_TIME = 2
NUMBER_RECEIVED_SAMPLES_TO_PLOT = 10

def initialize_received_samples_file(): 
    initializator = 0.0
    file_id = open("received_samples.txt","w+")
    for index in range(NUMBER_RECEIVED_SAMPLES_TO_PLOT):
        file_id.write(f"{initializator},{initializator}\n")
    file_id.close()


def update_samples_file(new_x, new_y, list_x, list_y):
    list_y.pop()
    list_y.insert(0, new_y)
    list_x.pop()
    list_x.insert(0, new_x)
    file_id = open("received_samples.txt", "w+")
    for index in range(len(list_y)):
        file_id.write(f"{list_x[i]},{list_y[i]}\n")
    file_id.close()


def read_user_values():
    read_data = open("menu_values.txt", "r").read()
    data_array = read_data.split("\n")
    close_program = int(data_array[0])
    sampling_frequency = int(data_array[1])
    number_samples_fft = int(data_array[2])
    signal_amplitude = float(data_array[3])
    signal_frequency = float(data_array[4])
    signal_dc_level = float(data_array[5])
    noise_range = float(data_array[6])

    list_user_values = [close_program, sampling_frequency, number_samples_fft,
                        signal_amplitude, signal_frequency, signal_dc_level, noise_range]
    return list_user_values


def read_last_msgs():
    file_id = open("received_samples.txt", "r").read()
    data_read = file_id.split('\n')
    file_id.close()
    data_read.pop()
    x_values = []
    y_values = []
    for each_line in data_read:
        x, y = each_line.split(',')
        x_values.append(np.float64(x))
        y_values.append(np.float64(y))
    list_values = [x_values, y_values]
    return list_values


def handle_received_msg(msg):
    msg = msg.decode('utf-8')
    msg = msg.split('\n')
    msg = msg[0].split(',')
    print(f"{msg[0]},{msg[1]}\n")
    amplitude_rcv = []
    frequency_rcv = []
    amplitude_rcv, frequency_rcv = read_last_msgs()
    print(f"{amplitude_rcv}")
    print(f"{frequency_rcv}")
    # amplitude_sent, frequency_sent = read_last_msgs("expected_samples.txt")

    # list_user_values = read_user_values()
    update_samples_file(np.float64(msg[0]), np.float64(msg[1]), amplitude_rcv, frequency_rcv)
    # update_samples_file(list_user_values[3], list_user_values[4],
    #                   amplitude_sent, frequency_sent, "expected_samples.txt")


def on_connect(client, userdata, flags, rc):
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(BROKER_TOPIC)

# The callback for when a PUBLISH message is received from the server.


def on_message(client, userdata, msg):
    # print(f"{msg.payload}")
    handle_received_msg(msg.payload)


def mqtt_handler():
    time.sleep(WAIT_PROGRAM_TIME)
    initialize_received_samples_file()
    client = mqtt.Client()
    client.username_pw_set(USERNAME, PASSWORD)
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(BROKER_NAME, BROKER_PORT_NUMBER, BROKER_COMMUN_TIMEOUT)
    client.loop_forever()
