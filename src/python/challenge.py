import sys
import threading
import time
import logging
from os import system, name
from subprocess import call
import serial
import time
import paho.mqtt.client as mqtt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

ser = serial.Serial('/dev/ttyUSB0', baudrate=115200, timeout=1)

CLOSE_PROGRAM_OPTION = 0
SAMPLE_FREQUENCY_OPTION = 1
NUMBER_SAMPLES_OPTION = 2
SIGNAL_AMPLITUDE_OPTION = 3
SIGNAL_FREQUENCY_OPTION = 4
SIGNAL_DC_LEVEL_OPTION = 5
NOISE_RANGE_OPTION = 6


MIN_SAMPLE_FREQUENCY_VALUE = 1
MAX_SAMPLE_FREQUENCY_VALUE = 1000000

MIN_NUMBER_SAMPLES_VALUE = 4
MAX_NUMBER_SAMPLES_VALUE = 32768

MIN_SIGNAL_AMPLITUDE_VALUE = 0.01
MAX_SIGNAL_AMPLITUDE_VALUE = 1000000

MIN_SIGNAL_FREQUENCY = 0.1

MAX_DC_LEVEL = 100000

MIN_NOISE_RANGE = 0
MAX_NOISE_RANGE = 100

# sampling frequency
sampling_frequency = 10000.0

# sampling time
sampling_time = 1/sampling_frequency

# signal amplitude
amplitude = 1

# signal frequency
signal_frequency = 60

# signal dc level
signal_dc_level = 0

# signal period
signal_period = 1/signal_frequency

# number samples per cicle
number_samples_per_cicle = int(sampling_frequency/signal_frequency)

# number samples to plot
number_samples_plot = 2*number_samples_per_cicle

# number samples to calculate the fft
number_samples_fft = 1024

# actual time
actual_time = 0

# noise range
noise_range = 0.2*amplitude

# Initialize a list of values with zeros
# This list will be used to store the signal values
# in a file and then plot the real time signal
list_signal = [0]*number_samples_plot
list_time = np.arange(0, number_samples_plot)*sampling_time

# Store each noise component squared
noise_squared = 0

# Store each signal component squared
signal_squared = 0

# Store each amplitude and frequency received
list_amplitude_rcv = [0]*15
list_frequency_rcv = [0]*15
list_amplitude_real = [0]*15
list_frequency_real = [0]*15
list_last_samples_rcv = list(range(15))

# variable to finish all threads
close_program = False

logging.basicConfig(
    level=logging.DEBUG,
    format='(%(threadName)-10s) %(message)s',
)

lock = threading.Lock()


def clear_screen():
    # for windows
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def send_float(value):
    # Convert the value to bytes
    byte_value = bytes(np.float32(value))
    # Send the bytes
    ser.write(byte_value)


def send_float_data(value):
    # Send 'd' to start the communication
    # 'd' stands for data
    ser.write(b'd')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'd')
        send_float(value)
        esp_data = ser.readline()


def send_sampling_frequency(value):
    # Send 'f' to start the communication
    ser.write(b'f')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'f')
        send_float(value)
        esp_data = ser.readline()


def send_number_samples(value):
    # Send 's' to start the communication
    ser.write(b's')
    # Send the data
    send_float(value)
    # verify the ack
    esp_data = ser.readline()
    while esp_data != b'a\n':
        ser.write(b'f')
        send_float(value)
        esp_data = ser.readline()


def serial_pc2esp():
    logging.debug('Starting...')
    global sampling_frequency
    global sampling_time
    global amplitude
    global signal_frequency
    global signal_dc_level
    global signal_period
    global number_samples_per_cicle
    global number_samples_plot
    global number_samples_fft
    global actual_time
    global noise_range
    global list_signal
    global list_time
    global noise_squared
    global signal_squared
    global close_program

    for x in range(10):
        send_sampling_frequency(sampling_frequency)
        send_number_samples(number_samples_fft)

    while close_program == False:
        # first acquire the mutex lock to access all variables contents
        lock.acquire()
        if actual_time == 0:
            send_sampling_frequency(sampling_frequency)
            send_number_samples(number_samples_fft)

        # calculate the signal value
        signal = amplitude * \
            np.sin(2*np.pi*signal_frequency*actual_time) + signal_dc_level
        # calculate the noise value
        noise = np.float64(np.random.normal(-noise_range, noise_range, 1))
        # add signal with the white noise
        final_signal = signal + noise
        # calculate the actual signal value squared
        signal_squared = signal_squared + signal**2
        # calculate the actual signal noise squared
        noise_squared = noise_squared + noise**2

        # pop the last signal value from the list
        list_signal.pop()
        # insert the new signal value calculated
        list_signal.insert(0, final_signal)

        # increment the time variable
        actual_time = actual_time + sampling_time

        # verify if actual_time is greater or equal
        # to one signal period
        if actual_time >= (signal_period - sampling_time):
            # turn the time variable zero
            actual_time = 0
            # calculate the signal noise ratio in dB
            snr = 10*np.log10(signal_squared/noise_squared)
            # turn signal_squared and noise_squared zero
            signal_squared = 0
            noise_squared = 0
            # print the value of signal to noise ratio
            # print(f"SNR = {snr}")
        # send the signal data
        send_float_data(final_signal)
        # sleep the sampling time
        time.sleep(sampling_time)
        # release the mutex lock
        lock.release()

    logging.debug('Exiting')


def update_signal_values():
    global sampling_frequency
    global sampling_time
    global amplitude
    global signal_frequency
    global signal_dc_level
    global signal_period
    global number_samples_per_cicle
    global number_samples_plot
    global number_samples_fft
    global actual_time
    global noise_range
    global list_signal
    global list_time
    global noise_squared
    global signal_squared

    # update the sampling time
    sampling_time = 1/sampling_frequency
    # update the signal period
    signal_period = 1/signal_frequency
    # update the number of samples per cicle
    number_samples_per_cicle = int(sampling_frequency/signal_frequency)
    # update the number of samples to plot
    number_samples_plot = 2*number_samples_per_cicle
    # actual time
    actual_time = 0

    # Initialize a list of values with zeros
    # This list will be used to store the signal values
    # in a file and then plot the real time signal
    list_signal = [0]*number_samples_plot
    list_time = np.arange(0, number_samples_plot)*sampling_time

    # Store each noise component squared
    noise_squared = 0

    # Store each signal component squared
    signal_squared = 0


def verify_user_input():
    global sampling_frequency
    global number_samples_fft
    global amplitude
    global signal_frequency
    global signal_dc_level
    global noise_range
    global close_program
    global list_amplitude_rcv
    global list_frequency_rcv

    user_input_menu_selection = input("Your value: > ")
    if user_input_menu_selection.isnumeric():
        user_input_menu_selection = int(user_input_menu_selection)
        if user_input_menu_selection < 0 or user_input_menu_selection > 6:
            print("Invalid input. Please select a valid NUMBER in the menu.")
            time.sleep(2)
        else:
            if user_input_menu_selection == CLOSE_PROGRAM_OPTION:
                close_program = True
            elif user_input_menu_selection == SAMPLE_FREQUENCY_OPTION:
                while 1:
                    clear_screen()
                    user_input = input(
                        "Type the new sampling frequency (integer only):")
                    if user_input.isnumeric():
                        user_input = float(user_input)
                        if user_input < MIN_SAMPLE_FREQUENCY_VALUE or user_input > MAX_SAMPLE_FREQUENCY_VALUE:
                            print("Invalid value for sampling frequency.")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            sampling_frequency = user_input
                            update_signal_values()
                            print(
                                f"New sample frequency inserted: {sampling_frequency} Hz")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again.")
                        time.sleep(2)
            elif user_input_menu_selection == NUMBER_SAMPLES_OPTION:
                while 1:
                    clear_screen()
                    user_input = input(
                        "Type a number of samples (should be a integer and a power of two):")
                    if user_input.isnumeric():
                        user_input = float(user_input)
                        if user_input < MIN_NUMBER_SAMPLES_VALUE or user_input > MAX_NUMBER_SAMPLES_VALUE:
                            print("Invalid value for number of samples.")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            number_samples_fft = user_input
                            allowed_samples_fft = 1

                            while allowed_samples_fft < number_samples_fft:
                                allowed_samples_fft *= 2

                            number_samples_fft = allowed_samples_fft
                            update_signal_values()
                            print(
                                f"New number of samples inserted: {number_samples_fft}")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again.")
                        time.sleep(2)
            elif user_input_menu_selection == SIGNAL_AMPLITUDE_OPTION:
                while 1:
                    clear_screen()
                    user_input = input("Type the new signal amplitude: ")
                    if user_input.replace('.', '', 1).isdigit():
                        user_input = float(user_input)
                        if user_input < MIN_SIGNAL_AMPLITUDE_VALUE or user_input > MAX_SIGNAL_AMPLITUDE_VALUE:
                            print("Invalid value for user input.")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            amplitude = user_input
                            update_signal_values()
                            print(
                                f"New signal amplitude inserted: {amplitude}")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again.")
                        time.sleep(2)
            elif user_input_menu_selection == SIGNAL_FREQUENCY_OPTION:
                while 1:
                    clear_screen()
                    user_input = input("Type the new signal frequency: ")
                    if user_input.replace('.', '', 1).isdigit():
                        user_input = float(user_input)
                        if user_input < MIN_SIGNAL_FREQUENCY:
                            print("Invalid value for signal frequency.")
                            time.sleep(2)
                        elif sampling_frequency < 2*user_input:
                            print("Ops. Do you remember the Nyquist theorem?")
                            print(
                                "Your sampling frequency is less than twice the signal frequency.")
                            print("Type a lesser signal frequency.")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            signal_frequency = user_input
                            update_signal_values()
                            print(
                                f"New signal frequency inserted: {signal_frequency} Hz")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again.")
                        time.sleep(2)
            elif user_input_menu_selection == SIGNAL_DC_LEVEL_OPTION:
                while 1:
                    clear_screen()
                    user_input = input(
                        "Type a DC level to be inserted in the signal: ")
                    if user_input.replace('.', '', 1).isdigit():
                        user_input = float(user_input)
                        if user_input < -MAX_DC_LEVEL or user_input > MAX_DC_LEVEL:
                            print("Invalid value for DC level")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            signal_dc_level = user_input
                            update_signal_values()
                            print(
                                f"New signal DC-level inserted: {signal_dc_level}")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again")
                        time.sleep(2)
            elif user_input_menu_selection == NOISE_RANGE_OPTION:
                while 1:
                    clear_screen()
                    print(
                        "The noise range is defined as a number to be multiplied by the signal amplitude")
                    print(
                        "E.g., noise_range = 0.1 means that the actual white noise can assume values")
                    print("between -0.1*amplitude and 0.1*amplitude")
                    user_input = input("Type a new signal noise range: ")
                    if user_input.replace('.', '', 1).isdigit():
                        user_input = float(user_input)
                        if user_input < MIN_NOISE_RANGE:
                            print("Invalid value for noise range.")
                            time.sleep(2)
                        elif user_input > MAX_NOISE_RANGE:
                            print("Invalid value for noise range.")
                            time.sleep(2)
                        else:
                            lock.acquire()
                            noise_range = user_input*amplitude
                            update_signal_values()
                            print(f"New noise range inserted: {noise_range}")
                            time.sleep(2)
                            lock.release()
                            break
                    else:
                        print("Your input is not a number. Try again.")
                        time.sleep(2)
    else:
        clear_screen()
        print("Invalid input. Please select a NUMBER between 0 and 4.")
        time.sleep(2)


def print_menu_options():
    global sampling_frequency
    global amplitude
    global signal_frequency
    global number_samples_fft
    global signal_dc_level
    global noise_range
    print("Instructions:")
    print("-------------")
    print("Type a NUMBER to choose different settings about the signal sent.")
    print("Type 0 if you want to close the program.")
    print(
        f"1. Select a different sampling frequency. Actual = {sampling_frequency} Hz")
    print(
        f"2. Select a different number of samples to compute the fft. Actual N = {number_samples_fft}")
    print(
        f"3. Select a different signal amplitude. Actual amplitude = {amplitude}")
    print(
        f"4. Select a different signal frequency. Actual frequency = {signal_frequency}")
    print(
        f"5. Select a different signal DC-Level. Actual DC-level = {signal_dc_level}")
    print(
        f"6. Select a different signal noise. Actual noise-range = {noise_range}")


def program_menu():
    global close_program

    print("Wait a moment to set the configurations...")
    time.sleep(3)
    while close_program == False:
        clear_screen()
        print_menu_options()
        verify_user_input()

# The callback for when the client receives a CONNACK response from the server.


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("desafio-doc88-leonardo")

# The callback for when a PUBLISH message is received from the server.


def on_message(client, userdata, msg):
    global list_amplitude_rcv
    global list_frequency_rcv
    global list_amplitude_real
    global list_frequency_real
    global amplitude
    global signal_frequency
    # print(msg.topic+" "+str(msg.payload))
    received_msg = msg.payload
    received_msg = received_msg.decode('utf-8')
    received_msg = received_msg.split('\n')
    amplitude_rcv, frequency_rcv = received_msg[0].split(',')
    list_amplitude_rcv.pop()
    list_amplitude_rcv.insert(0, np.float64(amplitude_rcv))
    list_frequency_rcv.pop()
    list_frequency_rcv.insert(0, np.float64(frequency_rcv))
    list_amplitude_real.pop()
    list_amplitude_real.insert(0, np.float64(amplitude))
    list_frequency_real.pop()
    list_frequency_real.insert(0, np.float64(signal_frequency))


def mqtt_handler():
    client = mqtt.Client()
    client.username_pw_set("wawkqqpw", password="0Vesn5GNKywV")
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect("m12.cloudmqtt.com", 15299, 60)
    client.loop_forever()


def time_graph():
    fig = plt.figure()
    ax1 = fig.add_subplot(2, 1, 1)
    ax2 = fig.add_subplot(2, 2, 3)
    ax3 = fig.add_subplot(2, 2, 4)

    def animation_time_graph(i):
        global list_signal
        global list_time
        global list_amplitude_rcv
        global list_frequency_rcv
        global list_amplitude_real
        global list_frequency_real
        global list_last_samples_rcv

        ax1.clear()
        ax2.clear()
        ax3.clear()

        ax1.set_xlabel("time (s)")
        ax1.set_ylabel("Signal amplitude")
        ax1.set_title("Signal sent")
        ax1.plot(list_time, list_signal)

        ax2.set_xlabel("samples")
        ax2.set_ylabel("Amplitude")
        ax2.set_title("Real amplitude x Estimated amplitude")
        l1, = ax2.plot(list_last_samples_rcv, list_amplitude_rcv)
        l2, = ax2.plot(list_last_samples_rcv, list_amplitude_real)
        l1.set_label('estimated')
        l2.set_label('real')
        ax2.legend()

        ax3.set_xlabel("samples")
        ax3.set_ylabel("Frequency")
        ax3.set_title("Real frequency x Estimated frequency")
        l3, = ax3.plot(list_last_samples_rcv, list_frequency_rcv)
        l4, = ax3.plot(list_last_samples_rcv, list_frequency_real)
        l3.set_label('estimated')
        l4.set_label('real')
        ax3.legend()

    ani = animation.FuncAnimation(fig, animation_time_graph, interval=250)
    plt.show()


def main():
    try:
        thread1 = threading.Thread(name='serial_pc2esp', target=serial_pc2esp)
        thread2 = threading.Thread(
            name='mqtt_handler', target=mqtt_handler, daemon=True)
        thread3 = threading.Thread(
            name='time_graph', target=time_graph, daemon=True)
        thread1.start()
        thread2.start()
        thread3.start()
        program_menu()
        raise KeyboardInterrupt
    except KeyboardInterrupt:
        close_program = True
        thread1.join()
        print("Finishing the program...")


if __name__ == '__main__':
    main()
