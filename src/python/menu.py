from os import system, name
from subprocess import call
import time

# definition of all menu constants and functions

CLOSE_PROGRAM_OPTION = 0
SAMPLE_FREQUENCY_OPTION = 1
NUMBER_SAMPLES_OPTION = 2
SIGNAL_AMPLITUDE_OPTION = 3
SIGNAL_FREQUENCY_OPTION = 4
SIGNAL_DC_LEVEL_OPTION = 5
NOISE_RANGE_OPTION = 6

# max selectable values by the user
CLOSE_PROGRAM_CHOOSED = 1

MIN_SAMPLE_FREQUENCY_VALUE = 1
MAX_SAMPLE_FREQUENCY_VALUE = 1000000

MIN_NUMBER_SAMPLES_VALUE = 4
MAX_NUMBER_SAMPLES_VALUE = 32768

MIN_SIGNAL_AMPLITUDE_VALUE = 0.01
MAX_SIGNAL_AMPLITUDE_VALUE = 1000000

MIN_SIGNAL_FREQUENCY = 0.1
MAX_SIGNAL_FREQUENCY = MAX_SAMPLE_FREQUENCY_VALUE/2

MAX_DC_LEVEL = 100000

MIN_NOISE_RANGE = 0
MAX_NOISE_RANGE = 100

# time in seconds to show the user a message
USER_MESSAGE_TIME = 1

# default values to initialize the files

SAMPLE_FREQUENCY_DEFAUT = 10000
NUMBER_SAMPLES_DEFAUT = 1024
SIGNAL_AMPLITUDE_DEFAUT = 1
SIGNAL_FREQUENCY_DEFAULT = 60
DC_LEVEL_DEFAUT = 1
NOISE_DEFAULT_VALUE = 0.1


def read_actual_menu_values():
    read_data = open("menu_values.txt", "r").read()
    data_array = read_data.split("\n")

    close_program = int(data_array[0])
    sampling_frequency = int(data_array[1])
    number_samples_fft = int(data_array[2])
    signal_amplitude = float(data_array[3])
    signal_frequency = float(data_array[4])
    signal_dc_level = float(data_array[5])
    noise_range = float(data_array[6])
    
    list_menu_values = [close_program, sampling_frequency, number_samples_fft,
                        signal_amplitude, signal_frequency, signal_dc_level, noise_range]
    return list_menu_values


def initialize_menu_values():
    file_id = open("menu_values.txt", "w+")
    init_values = [0, SAMPLE_FREQUENCY_DEFAUT, NUMBER_SAMPLES_DEFAUT,
                   SIGNAL_AMPLITUDE_DEFAUT, SIGNAL_FREQUENCY_DEFAULT, DC_LEVEL_DEFAUT, 
                   NOISE_DEFAULT_VALUE]
    for index in range(len(init_values)):
        file_id.write(f"{init_values[index]}\n")
    file_id.close()

def write_actual_menu_values(list_menu_values):
    file_id = open("menu_values.txt", "w+")
    for index in range(len(list_menu_values)):
        file_id.write(f"{list_menu_values[index]}\n")
    file_id.close()


def update_menu_values(user_input_menu_selection, user_input):
    list_menu_values = read_actual_menu_values()
    list_menu_values[user_input_menu_selection] = user_input
    write_actual_menu_values(list_menu_values)


def clear_screen():
    # for windows
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def handle_sample_frequency_option(user_input_menu_selection):
    while 1:
        print("Remainder: The sampling frequency should be at least twice the signal frequency (Nyquist theorem)")
        user_input = input(
            "Type the new sampling frequency (integer only): ")
        if user_input.isnumeric():
            user_input = int(user_input)
            if user_input < MIN_SAMPLE_FREQUENCY_VALUE or user_input > MAX_SAMPLE_FREQUENCY_VALUE:
                print("Invalid value for sampling frequency.")
                time.sleep(USER_MESSAGE_TIME)
            else:
                update_menu_values(user_input_menu_selection, user_input)
                break
        else:
            print("Your input is not a number. Try again.")
            time.sleep(USER_MESSAGE_TIME)


def handle_number_samples_option(user_input_menu_selection):
    while 1:
        user_input = input(
            "Type a number of samples (should be a integer and a power of two): ")
        if user_input.isnumeric():
            user_input = float(user_input)
            if user_input < MIN_NUMBER_SAMPLES_VALUE or user_input > MAX_NUMBER_SAMPLES_VALUE:
                print("Invalid value for number of samples.")
                time.sleep(USER_MESSAGE_TIME)
            else:
                # update the number of samples with a correct
                # value if the user insists to use a number
                # which is not a power of two
                number_samples_fft = user_input
                allowed_samples_fft = 1

                while allowed_samples_fft < number_samples_fft:
                    allowed_samples_fft *= 2

                number_samples_fft = allowed_samples_fft
                update_menu_values(
                    user_input_menu_selection, number_samples_fft)
                time.sleep(USER_MESSAGE_TIME)
                break
        else:
            print("Your input is not a number. Try again.")
            time.sleep(USER_MESSAGE_TIME)


def handle_signal_amplitude_option(user_input_menu_selection):
    while 1:
        user_input = input("Type the new signal amplitude: ")
        # The next statement verifies if the number inserted is a float
        if user_input.replace('.', '', 1).isdigit():
            user_input = float(user_input)
            if user_input < MIN_SIGNAL_AMPLITUDE_VALUE or user_input > MAX_SIGNAL_AMPLITUDE_VALUE:
                print("Invalid value for user input.")
                time.sleep(USER_MESSAGE_TIME)
            else:
                update_menu_values(
                    user_input_menu_selection, user_input)
                time.sleep(USER_MESSAGE_TIME)
                break
        else:
            print("Your input is not a number. Try again.")
            time.sleep(USER_MESSAGE_TIME)


def handle_signal_frequency_option(user_input_menu_selection):
    while 1:
        print("Remainder: The sampling frequency should be at least twice the signal frequency (Nyquist theorem)")
        user_input = input("Type the new signal frequency: ")
        if user_input.replace('.', '', 1).isdigit():
            user_input = float(user_input)
            if user_input < MIN_SIGNAL_FREQUENCY or user_input > MAX_SIGNAL_FREQUENCY:
                print("Invalid value for signal frequency.")
                time.sleep(USER_MESSAGE_TIME)
            else:
                update_menu_values(user_input_menu_selection, user_input)
                time.sleep(USER_MESSAGE_TIME)
                break
        else:
            print("Your input is not a number. Try again.")
            time.sleep(USER_MESSAGE_TIME)


def handle_signal_dc_level_option(user_input_menu_selection):
    while 1:
        user_input = input(
            "Type a DC level to be inserted in the signal: ")
        if user_input.replace('.', '', 1).isdigit():
            user_input = float(user_input)
            if user_input < -MAX_DC_LEVEL or user_input > MAX_DC_LEVEL:
                print("Invalid value for DC level")
                time.sleep(USER_MESSAGE_TIME)
            else:
                update_menu_values(user_input_menu_selection, user_input)
                time.sleep(USER_MESSAGE_TIME)
                break
        else:
            print("Your input is not a number. Try again")
            time.sleep(USER_MESSAGE_TIME)


def handle_noise_range_option(user_input_menu_selection):
    while 1:
        user_input = input("Type a new signal noise range: ")
        if user_input.replace('.', '', 1).isdigit():
            user_input = float(user_input)
            if user_input < MIN_NOISE_RANGE:
                print("Invalid value for noise range.")
                time.sleep(USER_MESSAGE_TIME)
            elif user_input > MAX_NOISE_RANGE:
                print("Invalid value for noise range.")
                time.sleep(USER_MESSAGE_TIME)
            else:
                update_menu_values(user_input_menu_selection, user_input)
                time.sleep(USER_MESSAGE_TIME)
                break
        else:
            print("Your input is not a number. Try again.")
            time.sleep(USER_MESSAGE_TIME)


def verify_user_input():
    user_input_menu_selection = input("Your value: > ")
    if user_input_menu_selection.isnumeric():
        user_input_menu_selection = int(user_input_menu_selection)
        if user_input_menu_selection < CLOSE_PROGRAM_OPTION or user_input_menu_selection > NOISE_RANGE_OPTION:
            print("Invalid input. Please select a valid NUMBER in the menu.")
            time.sleep(USER_MESSAGE_TIME)
        else:
            clear_screen()
            if user_input_menu_selection == CLOSE_PROGRAM_OPTION:
                update_menu_values(user_input_menu_selection,
                                   CLOSE_PROGRAM_CHOOSED)
            elif user_input_menu_selection == SAMPLE_FREQUENCY_OPTION:
                handle_sample_frequency_option(user_input_menu_selection)
            elif user_input_menu_selection == NUMBER_SAMPLES_OPTION:
                handle_number_samples_option(user_input_menu_selection)
            elif user_input_menu_selection == SIGNAL_AMPLITUDE_OPTION:
                handle_signal_amplitude_option(user_input_menu_selection)
            elif user_input_menu_selection == SIGNAL_FREQUENCY_OPTION:
                handle_signal_frequency_option(user_input_menu_selection)
            elif user_input_menu_selection == SIGNAL_DC_LEVEL_OPTION:
                handle_signal_dc_level_option(user_input_menu_selection)
            elif user_input_menu_selection == NOISE_RANGE_OPTION:
                handle_noise_range_option(user_input_menu_selection)
    else:
        clear_screen()
        print("Invalid input. Please select a NUMBER between 0 and 6.")
        time.sleep(USER_MESSAGE_TIME)


def print_menu_options():
    [close_program, sampling_frequency,
     number_samples_fft, signal_amplitude, signal_frequency, signal_dc_level, noise_range] = read_actual_menu_values()
    print("Instructions:")
    print("-------------")
    print("Type a NUMBER to choose different settings about the signal sent.")
    print("Type 0 if you want to close the program.")
    print(
        f"1. Select a different sampling frequency. Actual = {sampling_frequency} Hz")
    print(
        f"2. Select a different number of samples to compute the fft. Actual N = {number_samples_fft}")
    print(
        f"3. Select a different signal amplitude. Actual amplitude = {signal_amplitude}")
    print(
        f"4. Select a different signal frequency. Actual frequency = {signal_frequency}")
    print(
        f"5. Select a different signal DC-Level. Actual DC-level = {signal_dc_level}")
    print(
        f"6. Select a different signal noise. Actual noise-range = {noise_range}")


def main_menu():
    initialize_menu_values()
    close_program = 0
    while close_program == 0:
        # clear_screen()
        print_menu_options()
        verify_user_input()
        # verify if the user want to close the program
        close_program, *_ = read_actual_menu_values()
    
