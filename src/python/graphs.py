import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

# Frame rate in miliseconds
FRAME_RATE = 200


def read_samples(filename):
    file_id = open(filename, "r").read()
    data_read = file_id.split('\n')
    x_values = []
    y_values = []
    for each_line in data_read:
        x, y = each_line.split(',')
        x_values.append(np.float64(x))
        y_values.append(np.float64(y))
    return (x_values, y_values)


def animation(i):

    time_values, signal_values = read_samples("sent_signal.txt")
    amplitude_rcv, frequency_rcv = read_samples("received_samples.txt")
    amplitude_sent, frequency_sent = read_samples("sent_samples.txt")
    sample_number = list(range(len(amplitude_rcv)))

    ax1.clear()
    ax2.clear()
    ax3.clear()

    ax1.set_xlabel("time (s)")
    ax1.set_ylabel("Signal amplitude")
    ax1.set_title("Signal sent")
    ax1.plot(time_values, signal_values)

    ax2.set_xlabel("samples")
    ax2.set_ylabel("Amplitude")
    ax2.set_title("Real amplitude x Estimated amplitude")
    l1, = ax2.plot(sample_number, amplitude_rcv)
    l2, = ax2.plot(sample_number, amplitude_sent)
    l1.set_label('estimated')
    l2.set_label('real')
    ax2.legend()

    ax3.set_xlabel("samples")
    ax3.set_ylabel("Frequency")
    ax3.set_title("Real frequency x Estimated frequency")
    l3, = ax3.plot(sample_number, frequency_rcv)
    l4, = ax3.plot(sample_number, frequency_sent)
    l3.set_label('estimated')
    l4.set_label('real')
    ax3.legend()


def plot_graphs():
    fig = plt.figure()
    ax1 = fig.add_subplot(2, 1, 1)
    ax2 = fig.add_subplot(2, 2, 3)
    ax3 = fig.add_subplot(2, 2, 4)

    ani = animation.FuncAnimation(
        fig, animation, interval=FRAME_RATE)
    plt.show()
