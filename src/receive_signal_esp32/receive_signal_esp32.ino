#include <arduinoFFT.h>

arduinoFFT FFT = arduinoFFT(); // Create FFT objects

// number of samples used
uint16_t number_samples = 0;
uint16_t received_samples = 0;
double sampling_frequency = 0;
double signal_value = 0;
double estimated_signal_amplitude = 0;
double estimated_signal_frequency = 0;

static double *signal_samples;
static double *v_real;
static double *v_imag;
char information[50] = {0};

void allocate_data(){
  if (received_samples != number_samples){
    number_samples = received_samples;
    delete signal_samples;
    delete v_real;
    delete v_imag;
    signal_samples = new double[number_samples]();
    v_real = new double[number_samples]();
    v_imag = new double[number_samples]();  
  }
}

void calculate_fft(){
  
  allocate_data();
  for (uint16_t i = number_samples-1; i > 0; i--){
      signal_samples[i] = signal_samples[i-1];
  }
  signal_samples[0] = signal_value;
//
  for (uint16_t i = 0; i < number_samples; i++){
    v_real[i] = signal_samples[i];
    v_imag[i] = 0.0;
    //sprintf(s_value,"Re: %lf\n",v_real[i]);
    //Serial.write(s_value);
    //sprintf(s_value,"Im: %lf\n",v_imag[i]);
    //Serial.write(s_value);
  }

  FFT.Windowing(v_real, number_samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  FFT.Compute(v_real, v_imag, number_samples, FFT_FORWARD);
  FFT.ComplexToMagnitude(v_real, v_imag, number_samples);
  FFT.MajorPeak(v_real, number_samples, sampling_frequency, &estimated_signal_frequency, &estimated_signal_amplitude);
  sprintf(information, "X: %lf f: %lf\n", (4*estimated_signal_amplitude)/number_samples, estimated_signal_frequency);
  //sprintf(information,"ok!!\n");
  Serial.write(information);
}

bool verify_signal_received(float value){
  if(isnan(value) || isinf(value) || (value > 1000000) || (value < -1000000)){
    return false;
  }
  return true;
}

bool verify_sample_frequency_received(float value){
  if(isnan(value) || isinf(value) || (value < 1) || (value > 1000000)){
    return false;
  }
  return true;
}

bool verify_number_samples_received(float value){
  if(isnan(value) || isinf(value) || (value < 1) || (value > 32768)){
    return false;
  }
  return true;
}

float receive_float(){
  unsigned char received_byte = 0, store_recv_bytes[4] = {0};
  float received_data = 0;
  char s_received_data[20] = {0};
  
  // Turn the received data bytes into 0
  for(int i = 0; i < 4; i++){
    store_recv_bytes[i] = Serial.read();
  }
  // Write the float bytes into the memory space
  // which the received_data variable is allocated
  memcpy(&received_data, store_recv_bytes, sizeof(float));
  // Send back to see if everything is ok
  //sprintf(s_received_data,"%f\n",received_data);
  //Serial.write(s_received_data);
  return received_data;
}

void verify_received_data(){
  if(Serial.available() > 0){
    //Read the incoming byte
    unsigned char received_byte = Serial.read();
    float received_float = 0;
    // Verify first if the data to come is a information about the sampling frequency or a signal data
    // When 'd' is received, signal data is coming. 
    // When 's' is received, information about sampling frequency is coming
    // When 's' is received, information about number of samples for the fft is coming
    
    if(received_byte == 'd'){
      received_float = receive_float();
      if (verify_signal_received(received_float)){
        signal_value = received_float;
        calculate_fft();
      }
      else{
        calculate_fft();
      }
    }
    else if(received_byte == 'f'){
      received_float = receive_float();
      if (verify_sample_frequency_received(received_float)){
        sampling_frequency = received_float;
      }
    }
    else if(received_byte == 's'){
      received_float = receive_float();
      if (verify_number_samples_received(received_float)){
        received_samples = (uint16_t)received_float;
      }
    }
  }
}

void setup() {
  Serial.begin(115200);
}

void loop() { 
  if(Serial.available() > 0){
    verify_received_data();
  }

}
