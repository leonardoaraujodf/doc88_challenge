import serial
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import csv

# fig = plt.figure()
# ax1 = fig.add_subplot(1,1,1)

# sampling frequency
fs = 1000
# sampling time
ts = 1/fs
# signal amplitude
amp = 100
# signal frequency
f = 10
# signal period
per = 1/f
# number samples per cicle
number_samples_per_cicle = int(fs/f)
# number samples to plot
number_samples_plot = 3*number_samples_per_cicle
# actual time vector
t = 0
# noise range
noise_range = 0.05*amp

# initialize a list of values with zeros
list_signal = [0]*number_samples_plot
time_vector = np.arange(0,number_samples_plot)*ts

# variable to compute each noise component squared
noise_squared = 0

# variable to compute each signal component squared
signal_squared = 0

while 1:
    signal = amp*np.sin(2*np.pi*f*t)
    noise = np.float64(np.random.normal(-noise_range,noise_range,1))
    final_signal = signal + noise

    noise_squared = noise_squared + noise**2
    signal_squared = signal_squared + signal**2

    list_signal.pop()
    list_signal.insert(0,final_signal)

    # with open('sinusoid_sent.csv','w') as my_file:
    #    csvwriter = csv.writer(my_file, delimiter = ',')
    #    csvwriter.writerow(list_time)
    #    csvwriter.writerow(list_signal)

    # print(signal)
    
    # increment the time variable
    t = t + ts

    # verify if one period has been computed
    if t >= (per - ts):
        # turn the time variable zero
        t = 0
        # calculate the signal noise ratio
        snr = 10*np.log10(signal_squared/noise_squared)
        signal_squared = 0
        noise_squared = 0
        # print(f"SNR = {snr}")
        file_id = open("sent_signal.txt","w+")
        for i in range(len(list_signal)):
            file_id.write(f"{time_vector[i]},{list_signal[i]}\n")
        file_id.close()
    time.sleep(ts)


# def animate(i):
#     t = i*ts
#     signal = amp*np.sin(2*np.pi*f*i)
#     ax1.clear()
#     ax1.plot(t,signal)
# 
# ani = animation.FuncAnimation(fig, animate, interval=fs)
# plt.show()