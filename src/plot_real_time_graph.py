import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
    pull_data = open("sent_signal.txt","r").read()
    data_array = pull_data.split('\n')
    time_values = []
    signal_values = []
    for each_line in data_array:
        if len(each_line) > 1:
            x,y = each_line.split(',')
            time_values.append(np.float64(x))
            signal_values.append(np.float64(y))
    ax1.clear()
    plt.xlabel("time (s)")
    plt.ylabel("Signal ampltiude")
    plt.title("Signal sent")
    ax1.plot(time_values,signal_values)
ani = animation.FuncAnimation(fig, animate, interval=100)
plt.show()